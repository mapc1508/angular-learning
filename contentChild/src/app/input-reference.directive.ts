import { Directive, HostListener } from '@angular/core';

@Directive({
  // the selector of the directive finds all input elements defined inside my-custom-input
  selector:'my-custom-input input'
})
export class InputReferenceDirective {

  focus = false;

  // using HostListener decorator to listen to events occuring on the host component that has this directive
  @HostListener('focus')
  onfocus() {
    this.focus = true;
  }

  @HostListener('blur')
  onBlur() {
    this.focus = false;
  }
}
