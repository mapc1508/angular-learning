import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyCustomInputComponent } from './my-custom-input/my-custom-input.component';
import { InputReferenceDirective } from './input-reference.directive';

@NgModule({
  declarations: [
    AppComponent,
    MyCustomInputComponent,
    InputReferenceDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
