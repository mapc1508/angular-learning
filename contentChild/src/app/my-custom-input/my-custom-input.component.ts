import { ViewEncapsulation, Input, Component, ContentChild, AfterContentInit, HostBinding } from '../../../node_modules/@angular/core';
import { InputReferenceDirective } from '../input-reference.directive';

@Component({
  selector: 'my-custom-input',
  templateUrl: 'my-custom-input.component.html',
  styleUrls: ['my-custom-input.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class MyCustomInputComponent implements AfterContentInit {

  @Input() label;

  @ContentChild(InputReferenceDirective)
  injectedInput: InputReferenceDirective;

  ngAfterContentInit() {
    console.log('Injected input element: ', this.injectedInput);

    if (!this.injectedInput) {
      // loggin error message if InputReferenceDirective is not present on the host component
      // using it as a guard to enforce consumer of the custom component to implement this functionality
      console.error('my-custom-input: no proper initialization: pass in a <input> element between <my-custom-input></my-custom-input> in your application');
    }
  }

  // applying styling to the host component whenever the focus of the injected component is active
  @HostBinding('class.input-focus')
  get isInputFocus() {
    return this.injectedInput ? this.injectedInput.focus : false;
  }

  constructor() {
  }
}
