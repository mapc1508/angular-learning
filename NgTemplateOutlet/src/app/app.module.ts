import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CardOrListViewComponent } from './card-or-list-view/card-or-list-view.component';
import { UsageComponent } from './usage/usage.component';
import { NgTemplateOutletExampleComponent } from './ng-template-outlet-example/ng-template-outlet-example.component';
import { CardItemDirective } from './card-item.directive';
import { ListItemDirective } from './list-item.directive';

@NgModule({
  declarations: [
    AppComponent,
    CardOrListViewComponent,
    UsageComponent,
    NgTemplateOutletExampleComponent,
    CardItemDirective,
    ListItemDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
