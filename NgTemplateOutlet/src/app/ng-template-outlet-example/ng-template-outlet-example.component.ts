import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ng-template-outlet-example',
  templateUrl: './ng-template-outlet-example.component.html',
  styleUrls: ['./ng-template-outlet-example.component.css']
})
export class NgTemplateOutletExampleComponent implements OnInit {

  exampleContext = {
    $implicit: 'default context property when none specified',
    aContextProperty: 'a context property',
    test: 'Mirza'
  };

  ngOnInit(): void {
  }

}
