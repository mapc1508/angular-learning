import { Component } from '@angular/core';

@Component({
  templateUrl: 'usage.component.html',
  selector: 'usage-example'
})
export class UsageComponent {
  mode = 'list';
  items = [
    {
      header: 'Creating Reuseable Components with NgTemplateOutlet in Angular',
      content: 'The single responsibility principle...'
    } // ... more items
  ];
}
