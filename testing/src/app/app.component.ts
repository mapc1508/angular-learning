import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  list: number[] = new Array<number>();

  color = 'yellow';
  show = true;

  constructor() {
    this.list.push(1);
    this.list.push(2);
    this.list.push(3);
    this.list.push(4);
    this.list.push(5);
  }

  addItem() {
    this.list.push(this.list[this.list.length - 1] + 1);
  }

  removeItem(id: number) {
    this.list = this.list.filter(x => x !== id);
  }

  resetList() {
    this.list = new Array<number>();
    this.list.push(1);
    this.list.push(2);
    this.list.push(3);
    this.list.push(4);
    this.list.push(5);
  }

  showHide() {
    this.show = !this.show;
  }
}
