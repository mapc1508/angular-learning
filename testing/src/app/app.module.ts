import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ChildComponentComponent } from './view-child-example/child-component/child-component.component';
import { ChildrenListDirective } from './view-child-example/children-list.directive';

@NgModule({
  declarations: [
    AppComponent,
    ChildComponentComponent,
    ChildrenListDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
