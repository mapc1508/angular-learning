import { Directive, AfterContentInit, ContentChildren, QueryList, ChangeDetectorRef } from '@angular/core';
import { ChildComponentComponent } from './child-component/child-component.component';

@Directive({
  selector: '[appChildrenList]'
})
export class ChildrenListDirective implements AfterContentInit {

  @ContentChildren(ChildComponentComponent) children: QueryList<ChildComponentComponent>;

  constructor(private cdRef: ChangeDetectorRef) { }

  listItems: ChildComponentComponent[] = new Array<ChildComponentComponent>();

  selectedId: number;

  ngAfterContentInit(): void {
    this.subscribeChildrenList();

    this.children.changes.subscribe(x => {
      this.children = x;
      this.subscribeChildrenList();

      if (this.listItems.filter(li => li.id === this.selectedId).length === 0 && this.listItems.length > 0) {
            this.listItems[0].selected.next(this.listItems[0].id);
            this.cdRef.detectChanges();
      }
    });
  }

  subscribeChildrenList() {
    this.listItems = this.children.toArray();

    this.listItems.forEach(r => {
        if (r.selected.observers.length === 0) {
          r.selected.subscribe(x => this.selectItem(x));
        }
    });
  }

  selectItem(id: number) {

    this.listItems.forEach(x => {
      x.isSelected = false;
    });

    this.listItems.filter(x => x.id === id)[0].isSelected = true;

    this.selectedId = id;
  }

}
