import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-child-component',
  templateUrl: './child-component.component.html',
  styleUrls: ['./child-component.component.css']
})
export class ChildComponentComponent implements OnInit {

  @Output() selected = new EventEmitter<number>();

  @Input() id: number;

  isSelected = false;

  constructor() { }

  ngOnInit() {
  }

  onSelect(id: number) {
    this.selected.emit(id);
  }

}
